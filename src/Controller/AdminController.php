<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 20/05/2019
 * Time: 02:27
 */

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Order;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AdminController extends AbstractController
{
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    public function users()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('App\Entity\User')->findAll();

        return View::create($users, Response::HTTP_OK, []);
    }

    public function products()
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('App\Entity\Product')->findAll();

        return View::create($products, Response::HTTP_OK, []);
    }

    public function deactivateUser($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App\Entity\User')->find($id);
        if (!$this->isAdmin($this->getUser()->getId()))
        {
            return View::create(array("code" => 403, 'message'=> "FORBIDDEN"), Response::HTTP_FORBIDDEN);
        }
        if (empty($user)) {
            return new View(array("code" => 404, 'message'=> "User can not be found"), Response::HTTP_NOT_FOUND);
        } elseif ($user->getIsActive() == false) {
            return new View(array("code" => 404, 'message'=> "User already deactivate"), Response::HTTP_BAD_REQUEST);
        } else {
            $user->setIsActive(false);
            $em->flush();
            return View::create($user, Response::HTTP_OK, []);
        }
    }

    public function activateUser($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App\Entity\User')->find($id);
        if (!$this->isAdmin($this->getUser()->getId()))
        {
            return View::create(array("code" => 403, 'message'=> "FORBIDDEN"), Response::HTTP_FORBIDDEN);
        }
        if (empty($user)) {
            return new View(array("code" => 404, 'message'=> "User can not be found"), Response::HTTP_NOT_FOUND);
        } elseif ($user->getIsActive() == true) {
            return new View(array("code" => 404, 'message'=> "User already active"), Response::HTTP_BAD_REQUEST);
        } else {
            $user->setIsActive(true);
            $em->flush();
            return View::create($user, Response::HTTP_OK, []);
        }
    }
    public function adminUserProfile($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App\Entity\User')->find($id);
        if ($user->hasRole('ROLE_PROVIDER')) {
            $products = $em->getRepository('App\Entity\Product')->findBy(['provider' => $user]);
            return View::create(array('user'=>$user,'products'=>$products), Response::HTTP_OK, []);
        }
        else {
            return View::create(array('user'=>$user,'products'=>[]), Response::HTTP_OK, []);
        }
    }

    public function adminSetUserProfilePicture($id, Request $request)
    {
        $user = $this->getDoctrine()->getRepository('App\Entity\User')->find($id);
        if (empty($user)) {
            return new View(array("code" => 404, "message" => "User can not be found"), Response::HTTP_NOT_FOUND);
        }

        if (!$this->isAdmin($this->getUser()->getId()))
        {
            return View::create(array("code" => 403, "message" => "FORBIDDEN"), Response::HTTP_FORBIDDEN);
        }
        try {
            $file = $request->files->get( 'picture' );
            if (empty($file))
            {
                return View::create(array("code" => 400, "message" => "Null value can not be send"), Response::HTTP_BAD_REQUEST, []);
            }
            $fileName = md5 ( uniqid () ) . '.' . $file->guessExtension ();
            $original_name = $file->getClientOriginalName ();
            $file->move ( $this->params->get( 'app.path.profile_images' ), $fileName );

            $user->setImage($fileName);
            $user->setUpdatedAt(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $array = array (
                "code" => 200,
                'status' => 1,
                'param' => $this->params->get('app.path.profile_images'),
                'entity' => $user
            );
            $response = View::create($array, Response::HTTP_OK, []);
            return $response;
        } catch ( Exception $e ) {
            $array = array("code" => 400, 'status'=> 0 );
            $response = View::create($array, Response::HTTP_BAD_REQUEST, []);
            return $response;
        }
    }

    public function adminEditUserProfile(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository('App\Entity\User')->find($id);
        if (empty($user)) {
            return new View(array("code" => 404, "message" => "User can not be found"), Response::HTTP_NOT_FOUND);
        }

        if (!$this->isAdmin($this->getUser()->getId()))
        {
            return View::create(array("code" => 403, "message" => "FORBIDDEN"), Response::HTTP_FORBIDDEN);
        }

        $firstname = $request->get( 'firstname' );
        $lastname = $request->get( 'lastname' );
        $address = $request->get( 'address' );
        $company = $request->get( 'company' );
        $mat_number = $request->get( 'mat_number' );
        $iban = $request->get( 'iban' );

        if ($request->get( 'birthdate' ) != null) {
            $birthdate = new \DateTime($request->get( 'birthdate' ));
            $user->setBirthdate($birthdate);
        }
        $user->setFirstname($firstname);
        $user->setLastname($lastname);
        $user->setAddress($address);
        $user->setCompany($company);
        $user->setMatNumber($mat_number);
        $user->setIban($iban);


        $em->flush();
        return View::create($user, Response::HTTP_OK, []);

    }

    public function addProductToShop(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $product = $this->getDoctrine()->getRepository('App\Entity\Product')->find($id);

        if (!$this->isAdmin($this->getUser()->getId()))
        {
            return View::create(array("code" => 403, "message" => "FORBIDDEN"), Response::HTTP_FORBIDDEN);
        }

        if (empty($product)) {
            return new View(array("code" => 404, "message" => "Product can not be found"), Response::HTTP_NOT_FOUND);
        }elseif ($product->getIsActive() == true) {
            return new View(array("code" => 404, 'message'=> "Product already in shop"), Response::HTTP_BAD_REQUEST);
        } else {
            if ($product->getSellingPrice() <= 0 && $product->getStock() <= 0) {
                return new View(array("code" => 404, 'message'=> "Please set a valid price and check your stock"), Response::HTTP_BAD_REQUEST);
            }
            if ($product->getSellingPrice() <= 0) {
                return new View(array("code" => 404, 'message'=> "Please set a valid price"), Response::HTTP_BAD_REQUEST);
            }
            if ($product->getStock() <= 0) {
                return new View(array("code" => 404, 'message'=> "Your stock is empty!"), Response::HTTP_BAD_REQUEST);
            }
            $product->setIsActive(true);
            $em->flush();
            return View::create($product, Response::HTTP_OK, []);
        }
    }

    public function removeProductFromShop(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $product = $this->getDoctrine()->getRepository('App\Entity\Product')->find($id);

        if (!$this->isAdmin($this->getUser()->getId()))
        {
            return View::create(array("code" => 403, "message" => "FORBIDDEN"), Response::HTTP_FORBIDDEN);
        }

        if (empty($product)) {
            return new View(array("code" => 404, "message" => "Product can not be found"), Response::HTTP_NOT_FOUND);
        }elseif ($product->getIsActive() == false) {
            return new View(array("code" => 404, 'message'=> "Product already not in shop"), Response::HTTP_BAD_REQUEST);
        } else {
            $product->setIsActive(false);
            $em->flush();
            return View::create($product, Response::HTTP_OK, []);
        }
    }

    public function adminProductShow($id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('App\Entity\Product')->find($id);
        return View::create($product, Response::HTTP_OK, []);
    }

    public function adminEditProduct(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $this->getDoctrine()->getRepository('App\Entity\Product')->find($id);
        if (empty($product)) {
            return new View(array("code" => 404, "message" => "Product can not be found"), Response::HTTP_NOT_FOUND);
        }

        if (!$this->isAdmin($this->getUser()->getId()))
        {
            return View::create(array("code" => 403, "message" => "FORBIDDEN"), Response::HTTP_FORBIDDEN);
        }

        $selling_price = $request->get( 'selling_price' );
        $stock = $request->get( 'stock' );

        $product->setSellingPrice($selling_price);
        $product->setStock($stock);


        $em->flush();
        return View::create($product, Response::HTTP_OK, []);

    }

    public function adminAddCategory(Request $request)
    {
        $category = new Category();
        $name = $request->get('name');

        if(empty($name))
        {
            return View::create(array("code" => 406, "message" => "NULL VALUES ARE NOT ALLOWED"), Response::HTTP_NOT_ACCEPTABLE);
        }
        else {
            $category->setName($name);

            try {
                $file = $request->files->get( 'picture' );
                if (empty($file))
                {
                    array("code" => 400, "message" => "Null value can not be send");
                }
                $fileName = md5 ( uniqid () ) . '.' . $file->guessExtension ();
                $original_name = $file->getClientOriginalName ();
                $file->move ( $this->params->get( 'app.path.category_images' ), $fileName );

                $category->setImage($fileName);
                $category->setUpdatedAt(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();
                $array = array (
                    "code" => 200,
                    'status' => 1,
                    'param' => $this->params->get('app.path.category_images'),
                    'entity' => $category
                );
                $response = View::create($array, Response::HTTP_OK, []);
                return $response;
            } catch ( Exception $e ) {
                $array = array("code" => 400, 'status'=> 0 );
                $response = View::create($array, Response::HTTP_BAD_REQUEST, []);
                return $response;
            }
        }
    }

    public function adminDashboardNumbers()
    {
        $providers = 0;
        $customers = 0;
        $em = $this->getDoctrine()->getManager();
        $products = count($em->getRepository('App\Entity\Product')->findAll());
        $users = $em->getRepository('App\Entity\User')->findAll();
        foreach ($users as $user) {
            if ($user->hasRole('ROLE_PROVIDER')) {
                $providers++;
            }
            if ($user->hasRole('ROLE_USER')) {
                $customers++;
            }
        }
        $orders = count($em->getRepository('App\Entity\Order')->findAll());
        $array = array(["products" => $products, "providers" => $providers, "orders" => $orders, "customers" => $customers]);
        return View::create($array, Response::HTTP_OK, []);
    }
    public function isAdmin($idUser){
        $user = $this->getDoctrine()->getRepository('App\Entity\User')->find($idUser);
        return (in_array("ROLE_ADMIN" ,$user->getRoles()))?true:false;
    }
}