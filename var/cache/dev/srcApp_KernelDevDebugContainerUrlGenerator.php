<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;
    private $defaultLocale;

    public function __construct(RequestContext $context, LoggerInterface $logger = null, string $defaultLocale = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        $this->defaultLocale = $defaultLocale;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = [
        '_twig_error_test' => [['code', '_format'], ['_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code' => '\\d+'], [['variable', '.', '[^/]++', '_format'], ['variable', '/', '\\d+', 'code'], ['text', '/_error']], [], []],
        'register' => [[], ['_format' => 'json', '_controller' => 'App\\Controller\\DefaultController::register'], [], [['text', '/register']], [], []],
        'api' => [[], ['_format' => 'json', '_controller' => 'App\\Controller\\DefaultController::api'], [], [['text', '/api']], [], []],
        'login_check' => [[], ['_format' => 'json'], [], [['text', '/login_check']], [], []],
        'SupplierRegister' => [[], ['_format' => 'json', '_controller' => 'App\\Controller\\SupplierController::SupplierRegister'], [], [['text', '/SupplierRegister']], [], []],
        'SupplierProductRequest' => [[], ['_format' => 'json', '_controller' => 'App\\Controller\\SupplierController::SupplierProductRequest'], [], [['text', '/SupplierProductRequest']], [], []],
        'users' => [[], ['_format' => 'json', '_controller' => 'App\\Controller\\AdminController::users'], [], [['text', '/users']], [], []],
        'deactivateUser' => [['id'], ['_format' => 'json', '_controller' => 'App\\Controller\\AdminController::deactivateUser'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/deactivateUser']], [], []],
        'activateUser' => [['id'], ['_format' => 'json', '_controller' => 'App\\Controller\\AdminController::activateUser'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/activateUser']], [], []],
        'adminUserProfile' => [['id'], ['_format' => 'json', '_controller' => 'App\\Controller\\AdminController::adminUserProfile'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/adminUserProfile']], [], []],
        'adminSetUserProfilePicture' => [['id'], ['_format' => 'json', '_controller' => 'App\\Controller\\AdminController::adminSetUserProfilePicture'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/adminSetUserProfilePicture']], [], []],
        'adminEditUserProfile' => [['id'], ['_format' => 'json', '_controller' => 'App\\Controller\\AdminController::adminEditUserProfile'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/adminEditUserProfile']], [], []],
        'addProductToShop' => [['id'], ['_format' => 'json', '_controller' => 'App\\Controller\\AdminController::addProductToShop'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/addProductToShop']], [], []],
        'removeProductFromShop' => [['id'], ['_format' => 'json', '_controller' => 'App\\Controller\\AdminController::removeProductFromShop'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/removeProductFromShop']], [], []],
        'products' => [[], ['_format' => 'json', '_controller' => 'App\\Controller\\AdminController::products'], [], [['text', '/products']], [], []],
        'adminProductShow' => [['id'], ['_format' => 'json', '_controller' => 'App\\Controller\\AdminController::adminProductShow'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/adminProductShow']], [], []],
        'adminEditProduct' => [['id'], ['_format' => 'json', '_controller' => 'App\\Controller\\AdminController::adminEditProduct'], [], [['variable', '/', '[^/]++', 'id'], ['text', '/adminEditProduct']], [], []],
        'productsPublicShow' => [[], ['_format' => 'json', '_controller' => 'App\\Controller\\PublicController::productsPublicShow'], [], [['text', '/productsPublicShow']], [], []],
    ];
        }
    }

    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH)
    {
        $locale = $parameters['_locale']
            ?? $this->context->getParameter('_locale')
            ?: $this->defaultLocale;

        if (null !== $locale && null !== $name) {
            do {
                if ((self::$declaredRoutes[$name.'.'.$locale][1]['_canonical_route'] ?? null) === $name) {
                    unset($parameters['_locale']);
                    $name .= '.'.$locale;
                    break;
                }
            } while (false !== $locale = strstr($locale, '_', true));
        }

        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
