<?php

use Symfony\Component\Routing\Matcher\Dumper\PhpMatcherTrait;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    use PhpMatcherTrait;

    public function __construct(RequestContext $context)
    {
        $this->context = $context;
        $this->staticRoutes = [
            '/register' => [[['_route' => 'register', '_format' => 'json', '_controller' => 'App\\Controller\\DefaultController::register'], null, ['POST' => 0], null, false, false, null]],
            '/api' => [[['_route' => 'api', '_format' => 'json', '_controller' => 'App\\Controller\\DefaultController::api'], null, null, null, false, false, null]],
            '/login_check' => [[['_route' => 'login_check', '_format' => 'json'], null, ['POST' => 0], null, false, false, null]],
            '/SupplierRegister' => [[['_route' => 'SupplierRegister', '_format' => 'json', '_controller' => 'App\\Controller\\SupplierController::SupplierRegister'], null, ['POST' => 0], null, false, false, null]],
            '/SupplierProductRequest' => [[['_route' => 'SupplierProductRequest', '_format' => 'json', '_controller' => 'App\\Controller\\SupplierController::SupplierProductRequest'], null, ['POST' => 0], null, false, false, null]],
            '/users' => [[['_route' => 'users', '_format' => 'json', '_controller' => 'App\\Controller\\AdminController::users'], null, ['GET' => 0], null, false, false, null]],
            '/products' => [[['_route' => 'products', '_format' => 'json', '_controller' => 'App\\Controller\\AdminController::products'], null, ['GET' => 0], null, false, false, null]],
            '/adminAddCategory' => [[['_route' => 'adminAddCategory', '_format' => 'json', '_controller' => 'App\\Controller\\AdminController::adminAddCategory'], null, ['POST' => 0], null, false, false, null]],
            '/adminDashboardNumbers' => [[['_route' => 'adminDashboardNumbers', '_format' => 'json', '_controller' => 'App\\Controller\\AdminController::adminDashboardNumbers'], null, ['GET' => 0], null, false, false, null]],
            '/productsPublicShow' => [[['_route' => 'productsPublicShow', '_format' => 'json', '_controller' => 'App\\Controller\\PublicController::productsPublicShow'], null, ['GET' => 0], null, false, false, null]],
            '/categoriesPublicShow' => [[['_route' => 'categoriesPublicShow', '_format' => 'json', '_controller' => 'App\\Controller\\PublicController::categoriesPublicShow'], null, ['GET' => 0], null, false, false, null]],
        ];
        $this->regexpList = [
            0 => '{^(?'
                    .'|/_error/(\\d+)(?:\\.([^/]++))?(*:35)'
                    .'|/deactivateUser/([^/]++)(*:66)'
                    .'|/a(?'
                        .'|ctivateUser/([^/]++)(*:98)'
                        .'|d(?'
                            .'|min(?'
                                .'|UserProfile/([^/]++)(*:135)'
                                .'|SetUserProfilePicture/([^/]++)(*:173)'
                                .'|Edit(?'
                                    .'|UserProfile/([^/]++)(*:208)'
                                    .'|Product/([^/]++)(*:232)'
                                .')'
                                .'|ProductShow/([^/]++)(*:261)'
                            .')'
                            .'|dProductToShop/([^/]++)(*:293)'
                        .')'
                    .')'
                    .'|/removeProductFromShop/([^/]++)(*:334)'
                    .'|/categoryPublicShow/([^/]++)(*:370)'
                .')/?$}sD',
        ];
        $this->dynamicRoutes = [
            35 => [[['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
            66 => [[['_route' => 'deactivateUser', '_format' => 'json', '_controller' => 'App\\Controller\\AdminController::deactivateUser'], ['id'], ['PUT' => 0], null, false, true, null]],
            98 => [[['_route' => 'activateUser', '_format' => 'json', '_controller' => 'App\\Controller\\AdminController::activateUser'], ['id'], ['PUT' => 0], null, false, true, null]],
            135 => [[['_route' => 'adminUserProfile', '_format' => 'json', '_controller' => 'App\\Controller\\AdminController::adminUserProfile'], ['id'], ['GET' => 0], null, false, true, null]],
            173 => [[['_route' => 'adminSetUserProfilePicture', '_format' => 'json', '_controller' => 'App\\Controller\\AdminController::adminSetUserProfilePicture'], ['id'], ['POST' => 0], null, false, true, null]],
            208 => [[['_route' => 'adminEditUserProfile', '_format' => 'json', '_controller' => 'App\\Controller\\AdminController::adminEditUserProfile'], ['id'], ['PUT' => 0], null, false, true, null]],
            232 => [[['_route' => 'adminEditProduct', '_format' => 'json', '_controller' => 'App\\Controller\\AdminController::adminEditProduct'], ['id'], ['PUT' => 0], null, false, true, null]],
            261 => [[['_route' => 'adminProductShow', '_format' => 'json', '_controller' => 'App\\Controller\\AdminController::adminProductShow'], ['id'], ['GET' => 0], null, false, true, null]],
            293 => [[['_route' => 'addProductToShop', '_format' => 'json', '_controller' => 'App\\Controller\\AdminController::addProductToShop'], ['id'], ['PUT' => 0], null, false, true, null]],
            334 => [[['_route' => 'removeProductFromShop', '_format' => 'json', '_controller' => 'App\\Controller\\AdminController::removeProductFromShop'], ['id'], ['PUT' => 0], null, false, true, null]],
            370 => [[['_route' => 'categoryPublicShow', '_format' => 'json', '_controller' => 'App\\Controller\\PublicController::categoryPublicShow'], ['id'], ['GET' => 0], null, false, true, null]],
        ];
    }
}
