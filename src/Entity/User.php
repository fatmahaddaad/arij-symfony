<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity
 */
class User implements UserInterface
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=500)
     * @Serializer\Exclude()
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=225, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="datetime", name="dateCreation")
     */
    private $dateCreation;

    /**
     * @ORM\Column(type="datetime", name="birthdate", nullable=true)
     */
    private $birthdate;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var array
     * @ORM\Column(type="array", length=500)
     */
    protected $roles;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="profile_pictures", fileNameProperty="image")
     */
    private $imageFile;

    /**
     *
     * @ORM\Column(type="datetime", name="updatedAt", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=225, nullable=true)
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=225, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=225, nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=225, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=225, nullable=true)
     */
    private $matNumber;

    /**
     * @ORM\Column(type="string", length=225, nullable=true)
     */
    private $iban;

    public function __construct($username)
    {
        $this->isActive = true;
        $this->username = $username;
        $this->roles = array('ROLE_USER');
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getSalt()
    {
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function eraseCredentials()
    {
    }

    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    public function getRoles()
    {
        $roles = $this->roles;
        return array_unique($roles);
    }

    public function addRole($role)
    {
        $role = strtoupper($role);

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }


    public function getId()
    {
        return $this->id;
    }


    public function getEmail()
    {
        return $this->email;
    }


    public function setEmail($email): void
    {
        $this->email = $email;
    }


    public function getDateCreation()
    {
        return $this->dateCreation;
    }


    public function setDateCreation($dateCreation): void
    {
        $this->dateCreation = $dateCreation;
    }


    public function getBirthdate()
    {
        return $this->birthdate;
    }


    public function setBirthdate($birthdate): void
    {
        $this->birthdate = $birthdate;
    }


    public function getisActive()
    {
        return $this->isActive;
    }


    public function setIsActive($isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }


    public function getCompany()
    {
        return $this->company;
    }


    public function setCompany($company): void
    {
        $this->company = $company;
    }


    public function getFirstname()
    {
        return $this->firstname;
    }


    public function setFirstname($firstname): void
    {
        $this->firstname = $firstname;
    }


    public function getLastname()
    {
        return $this->lastname;
    }


    public function setLastname($lastname): void
    {
        $this->lastname = $lastname;
    }


    public function getAddress()
    {
        return $this->address;
    }


    public function setAddress($address): void
    {
        $this->address = $address;
    }


    public function getMatNumber()
    {
        return $this->matNumber;
    }


    public function setMatNumber($matNumber): void
    {
        $this->matNumber = $matNumber;
    }


    public function getIban()
    {
        return $this->iban;
    }


    public function setIban($iban): void
    {
        $this->iban = $iban;
    }


}
