<?php return array (
  'register' => 
  array (
    0 => 'POST',
  ),
  'login_check' => 
  array (
    0 => 'POST',
  ),
  'SupplierRegister' => 
  array (
    0 => 'POST',
  ),
  'SupplierProductRequest' => 
  array (
    0 => 'POST',
  ),
  'users' => 
  array (
    0 => 'GET',
  ),
  'deactivateUser' => 
  array (
    0 => 'PUT',
  ),
  'activateUser' => 
  array (
    0 => 'PUT',
  ),
  'adminUserProfile' => 
  array (
    0 => 'GET',
  ),
  'adminSetUserProfilePicture' => 
  array (
    0 => 'POST',
  ),
  'adminEditUserProfile' => 
  array (
    0 => 'PUT',
  ),
  'addProductToShop' => 
  array (
    0 => 'PUT',
  ),
  'removeProductFromShop' => 
  array (
    0 => 'PUT',
  ),
  'products' => 
  array (
    0 => 'GET',
  ),
  'adminProductShow' => 
  array (
    0 => 'GET',
  ),
  'adminEditProduct' => 
  array (
    0 => 'PUT',
  ),
  'adminAddCategory' => 
  array (
    0 => 'POST',
  ),
  'adminDashboardNumbers' => 
  array (
    0 => 'GET',
  ),
  'productsPublicShow' => 
  array (
    0 => 'GET',
  ),
  'categoriesPublicShow' => 
  array (
    0 => 'GET',
  ),
  'categoryPublicShow' => 
  array (
    0 => 'GET',
  ),
);