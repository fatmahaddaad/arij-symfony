<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 11/05/2019
 * Time: 13:26
 */

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
/**
 * @ORM\Table(name="product")
 * @ORM\Entity
 */

class Product
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="provider_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $provider;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $description;

    /**
     * @var array
     * @ORM\Column(type="array", length=500, nullable=true)
     */
    private $sizes;

    /**
     * @var array
     * @ORM\Column(type="array", length=500, nullable=true)
     */
    private $colors;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $sellingPrice;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stock;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $deliveryType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $deliveryTime;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $deliveryPrice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="product_pictures", fileNameProperty="image")
     */
    private $imageFile;

    /**
     *
     * @ORM\Column(type="datetime", name="updatedAt", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="datetime", name="dateCreation")
     */
    private $dateCreation;

    public function getProvider()
    {
        return $this->provider;
    }

    public function setProvider($provider): void
    {
        $this->provider = $provider;
    }

    public function __construct()
    {
        $this->isActive = false;
        $this->sellingPrice = 0;
        $this->stock = 0;
        $this->colors = null;
        $this->sizes = null;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label): void
    {
        $this->label = $label;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type): void
    {
        $this->type = $type;
    }

    public function hasSize($size)
    {
        return in_array(strtoupper($size), $this->getSizes(), true);
    }

    public function getSizes()
    {
        $sizes = $this->sizes;
        return array_unique($sizes);
    }

    public function addSize($size)
    {
        $size = strtoupper($size);

        if ($this->sizes) {
            if (!in_array($size, $this->sizes, true)) {
                $this->sizes[] = $size;
            }
        } else {
            $this->sizes[] = $size;
        }
        return $this;
    }

    public function removeSize($size)
    {
        if (false !== $key = array_search(strtoupper($size), $this->sizes, true)) {
            unset($this->sizes[$key]);
            $this->sizes = array_values($this->sizes);
        }

        return $this;
    }

    public function hasColor($color)
    {
        return in_array(strtoupper($color), $this->getColors(), true);
    }

    public function getColors()
    {
        $colors = $this->colors;
        return array_unique($colors);
    }

    public function addColor($color)
    {
        $color = strtoupper($color);

        if ($this->colors) {
            if (!in_array($color, $this->colors, true)) {
                $this->colors[] = $color;
            }
        } else {
            $this->colors[] = $color;
        }

        return $this;
    }

    public function removeColor($color)
    {
        if (false !== $key = array_search(strtoupper($color), $this->colors, true)) {
            unset($this->colors[$key]);
            $this->colors = array_values($this->colors);
        }

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): void
    {
        $this->price = $price;
    }

    public function getDeliveryType()
    {
        return $this->deliveryType;
    }

    public function setDeliveryType($deliveryType): void
    {
        $this->deliveryType = $deliveryType;
    }

    public function getDeliveryTime()
    {
        return $this->deliveryTime;
    }

    public function setDeliveryTime($deliveryTime): void
    {
        $this->deliveryTime = $deliveryTime;
    }

    public function getDeliveryPrice()
    {
        return $this->deliveryPrice;
    }

    public function setDeliveryPrice($deliveryPrice): void
    {
        $this->deliveryPrice = $deliveryPrice;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getisActive()
    {
        return $this->isActive;
    }

    public function setIsActive($isActive): void
    {
        $this->isActive = $isActive;
    }

    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    public function setDateCreation($dateCreation): void
    {
        $this->dateCreation = $dateCreation;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): void
    {
        $this->description = $description;
    }

    public function getSellingPrice()
    {
        return $this->sellingPrice;
    }

    public function setSellingPrice($sellingPrice): void
    {
        $this->sellingPrice = $sellingPrice;
    }

    public function getStock()
    {
        return $this->stock;
    }

    public function setStock($stock): void
    {
        $this->stock = $stock;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category): void
    {
        $this->category = $category;
    }

}