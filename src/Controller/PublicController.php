<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 29/05/2019
 * Time: 10:55
 */

namespace App\Controller;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use App\Entity\Product;

class PublicController extends AbstractController
{
    public function productsPublicShow() {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('App\Entity\Product')->findBy(["isActive" => true]);

        return View::create($products, Response::HTTP_OK, []);
    }

    public function categoriesPublicShow() {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('App\Entity\Category')->findAll();

        return View::create($categories, Response::HTTP_OK, []);
    }

    public function categoryPublicShow($id) {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('App\Entity\Category')->find($id);

        return View::create($category, Response::HTTP_OK, []);
    }
}