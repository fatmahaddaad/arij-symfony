<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 15/05/2019
 * Time: 03:46
 */

namespace App\Controller;

use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use App\Entity\Product;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SupplierController extends AbstractController
{
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    public function SupplierRegister(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user = new User($request->get('username'));
        $user->setPassword($encoder->encodePassword($user, $request->get('password')));
        $user->setEmail($request->get('email'));
        if ($request->get('firstname')) {
            $user->setFirstname($request->get('firstname'));
        }
        if ($request->get('lastname')) {
            $user->setLastname($request->get('lastname'));
        }
        if ($request->get('company')) {
            $user->setCompany($request->get('company'));
        }
        if ($request->get('address')) {
            $user->setAddress($request->get('address'));
        }
        if ($request->get('matnumber')) {
            $user->setMatNumber($request->get('matnumber'));
        }
        if ($request->get('iban')) {
            $user->setIban($request->get('iban'));
        }

        $user->setIsActive(false);
        $user->addRole('ROLE_PROVIDER');
        $user->setDateCreation(new \DateTime());
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return View::create($user, Response::HTTP_CREATED, []);
    }

    public function SupplierProductRequest(Request $request)
    {
        $product = new Product();
        $label = $request->get('label');
        $type = $this->getDoctrine()->getRepository('App\Entity\Category')->find($request->get('type'));;
        $sizes = $request->get('sizes');
        $colors = $request->get('colors');
        $price = $request->get('price');
        $delivery_type = $request->get('delivery_type');
        $delivery_time = $request->get('delivery_time');
        $delivery_price = $request->get('delivery_price');
        $selling_price = 0;
        $stock = $request->get('stock');
        $description = $request->get('description');
        $dateCreation = new \DateTime();
        $provider = $this->getUser();

        if(empty($label) || empty($price))
        {
            return View::create(array("code" => 406, "message" => "NULL VALUES ARE NOT ALLOWED"), Response::HTTP_NOT_ACCEPTABLE);
        }
        else {
            $product->setLabel($label);
            $product->setType($type);
            $product->setCategory($type);
            $product->addSize($sizes);
            $product->addColor($colors);
            $product->setPrice($price);
            $product->setDeliveryType($delivery_type);
            $product->setDeliveryTime($delivery_time);
            $product->setDeliveryPrice($delivery_price);
            $product->setDateCreation($dateCreation);
            $product->setProvider($provider);
            $product->setSellingPrice($selling_price);
            $product->setStock($stock);
            $product->setDescription($description);

            try {
                $file = $request->files->get( 'picture' );
                if (empty($file))
                {
                    array("code" => 400, "message" => "Null value can not be send");
                }
                $fileName = md5 ( uniqid () ) . '.' . $file->guessExtension ();
                $original_name = $file->getClientOriginalName ();
                $file->move ( $this->params->get( 'app.path.product_images' ), $fileName );

                $product->setImage($fileName);
                $product->setUpdatedAt(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($product);
                $em->flush();
                $array = array (
                    "code" => 200,
                    'status' => 1,
                    'param' => $this->params->get('app.path.product_images'),
                    'entity' => $product
                );
                $response = View::create($array, Response::HTTP_OK, []);
                return $response;
            } catch ( Exception $e ) {
                $array = array("code" => 400, 'status'=> 0 );
                $response = View::create($array, Response::HTTP_BAD_REQUEST, []);
                return $response;
            }

//            $em = $this->getDoctrine()->getManager();
//            $em->persist($product);
//            $em->flush();
//
//            return View::create($product, Response::HTTP_CREATED, []);
        }
    }
}